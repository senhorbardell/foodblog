<?php

function veggy_setup() {
	add_theme_support('post-thumbnails');

	add_theme_support( 'featured-content', array(
		'featured_content_filter' => 'twentyfourteen_get_featured_posts',
		'max_posts' => 6,
	));

	set_post_thumbnail_size(1000, 600, true);

	add_image_size('veggy_full-width', 1038, 576, true);

	register_nav_menus(array(
		'top' => 'Top primary',
		'lover' => 'Footer menu'
		));

	add_theme_support('html5', array('search-form', 'comment-form', 'comment-list'));


}
add_action('after_setup_theme', 'veggy_setup');

function veggy_scripts() {
	wp_enqueue_style('veggy-style', get_template_directory_uri().'/css/app.css');
	wp_enqueue_script(
		'jquery',
		get_template_directory_uri().'/bower_components/jquery/jquery.min.js',
		array(),
		false,
		true
	);
	wp_enqueue_script(
		'foundation', 
		get_template_directory_uri(). '/bower_components/foundation/js/foundation.min.js',
		array('jquery'),
		false,
		true
	);
	wp_enqueue_script(
		'foundation-topbar',
		get_template_directory_uri().'/bower_components/foundation/js/foundation/foundation.topbar.js',
		array('foundation'),
		false,
		true
	);
	wp_enqueue_script(
		'veggy-script', 
		get_template_directory_uri().'/js/app.js',
		array('foundation', 'foundation-topbar', 'jquery'),
		false,
		true
	);
}
add_action('wp_enqueue_scripts', 'veggy_scripts');

/**
	* Featured Content functionality
	*/
if (!class_exists('Featured_Content') && 'plugins.php' !== $GLOBALS['pagenow']) {
	require get_template_directory() . '/inc/featured-content.php';
}

/**
	* Getter function for Featured Content Plugin
	*
	* @since Twenty Fourteen 1.0
	*
	* @param array An array of WP_Post objects.
	*/
function twentyfourteen_get_featured_posts() {
	/**
		* Filter the featured posts to return in Twenty Fourteen.
		*
		* @since Twenty Fourteen 1.0
		*
		* @param array|bool $posts Array of featured posts, otherwise false
		*/
		return apply_filters('twentyfourteen_get_featured_posts', array());
}
/**
 * A helper conditional function that returns a boolean value.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return bool Whether there are featured posts.
 */
function twentyfourteen_has_featured_posts() {
	return ! is_paged() && (bool) twentyfourteen_get_featured_posts();
}

if ( ! function_exists( 'veggy_posted_on' ) ) :
/**
 * Print HTML with meta information for the current post-date/time and author.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return void
 */
function veggy_posted_on() {
	if ( is_sticky() && is_home() && ! is_paged() ) {
		echo '<span class="featured-post">' . __( 'Прикрепрённый', 'veggy' ) . '</span> ';
	}

	// Set up and print post meta information.
	printf( '<span class="entry-date">
			<a href="%1$s" rel="bookmark">
				<time class="entry-date" datetime="%2$s">
					%3$s
				</time>
			</a>
			</span> 
			<span class="byline">
				<span class="author vcard">
					<a class="url fn n" href="%4$s" rel="author">
						%5$s
					</a>
				</span>
			</span>',
		esc_url( get_permalink() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		get_the_author()
	);
}
endif;

if (!function_exists('veggy_pagination')) :
/**
	* Display navigation to next/prev set of posts when applicable
	*
	* @since Veggy 1.0
	* @return void
	*/
function veggy_pagination() {
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}

	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );

	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}

	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

	$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

	// Set up paginated links.
	$links = paginate_links( array(
		'base'     => $pagenum_link,
		'format'   => $format,
		'total'    => $GLOBALS['wp_query']->max_num_pages,
		'current'  => $paged,
		'mid_size' => 1,
		'add_args' => array_map( 'urlencode', $query_args ),
		'prev_text' => __( '&larr; Назад', 'veggy' ),
		'next_text' => __( 'Вперед &rarr;', 'veggy' ),
	) );

	if ( $links ) :

	?>
	<nav class="navigation paging-navigation" role="navigation">
		<!-- <h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'twentyfourteen' ); ?></h1> -->
		<div class="pagination loop-pagination">
			<?php echo $links; ?>
		</div><!-- .pagination -->
	</nav><!-- .navigation -->
	<?php
	endif;
}
endif;