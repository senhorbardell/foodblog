<?php
/** 
	* Page
	* @package Wordpress
	* @subpackage veggy
	* @since Veggy 0.0.0
	*/
?>

<?php get_header() ?>

<?php if (is_front_page() && twentyfourteen_has_featured_posts()) : ?>

	<section class="featured large-block-grid-3 medium-block-grid-3">
		<?php get_template_part('content', 'featured') ?>
	</section>

<?php endif ?>

<section class="main">

<?php while (have_posts()) : the_post(); ?>

	<?php get_template_part('content', 'page') ?>

	<?php if (comments_open() || get_comments_number() ) comments_template() ?>

<?php endwhile ?>

</section>

<?php get_footer() ?>