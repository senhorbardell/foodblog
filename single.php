<?php
/**
	* Single post
	* @package Wordpress
	* @subpackage veggy
	* @since Veggy 0.0.0
	*/
?>

<?php get_header() ?>

<section class="main">


		<?php while (have_posts()) : the_post(); ?>

				<?php get_template_part('content', get_post_format()) ?>

				<?php #@ToDO previos, next post nav ?>

				<?php if (comments_open() || get_comments_number() ) comments_template() ?>

		<?php endwhile ?>

</section>

<?php get_footer() ?>

