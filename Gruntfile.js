module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'js/app.js',
        dest: 'js/app.min.js'
      }
    },

    compass: {
      dist: {
        options: {
          sassDir: 'sass',
          cssDir: 'css',
          environment: 'production'
        }
      }
    },

    'ftp-deploy': {
      build: {
        auth: {
          host: 'direct.senhorbardell.com',
          authKey: 'blog'
        },
        src: '.',
        dest: '/dolcevita/public_html/wp-content/themes/veggy',
        exclusions: ['bower_components', 'sass', 'node_modules', '.grunt', '.git', '.sass-cache','bower.json', 'Gruntfile.js', 'package.json', '.ftppass', 'README.md', '.bowerrc', '.gitignore', 'veggy.sublime-project', 'veggy.sublime-workspace', 'config.rb']
      }
    },

    'ftpush': {
      build: {
        auth: {
          host: 'direct.senhorbardell.com',
          authKey: 'blog'
        },
        src: '.',
        dest: '/dolcevita/public_html/wp-content/themes/veggy',
        exclusions: ['bower_components', 'sass', 'node_modules', '.grunt', '.git', '.sass-cache','bower.json', 'Gruntfile.js', 'package.json', '.ftppass', 'README.md', '.bowerrc', '.gitignore', 'veggy.sublime-project', 'veggy.sublime-workspace', 'config.rb']
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Compass
  grunt.loadNpmTasks('grunt-contrib-compass');

  // Ftp Deploy
  grunt.loadNpmTasks('grunt-ftp-deploy');

  grunt.loadNpmTasks('grunt-ftpush');

  // Default task(s).
  grunt.registerTask('default', ['uglify', 'compass', 'ftpush']);

};