<nav class="top-bar" data-topbar>
  <ul class="title-area">
    <li class="name">
      <h1><a href="/foodblog">Foodblog</a></h1>
    </li>
    <li class="toggle-topbar menu-icon"><a href="#">Меню</a></li>
  </ul>

  <section class="top-bar-section">
    <?php wp_nav_menu(array(
      'theme-location' => 'top',
      'container' => '',
      'menu_class' => 'left'
    )) ?>

    <ul class="right">
      <li class="has-form"><?php get_search_form(); ?></li>
    </ul>
  </section>

</nav>