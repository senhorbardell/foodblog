<?php
/**
  * Content
  * @package Wordpress
  * @subpackage veggy
  * @since Veggy 0.0.0
  */
?>

<article <?php post_class() ?>>

	<header class="entry-header">
		<?php the_title('<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>') ?>

		<div class="entry-meta">
			<span class="cat-links"><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ); ?></span>
		</div>
		
		<div class="entry-meta">
			<?php
				if ( 'post' == get_post_type() )
					veggy_posted_on();

				if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
			?>
			<span class="comments-link"><?php comments_popup_link( __( 'Оставьте комментарий', 'veggy' ), __( '1 Комментарий', 'veggy' ), __( '% Комментариев', 'veggy' ) ); ?></span>
			<?php
				endif;

				edit_post_link( __( 'Редактировать', 'veggy' ), '<span class="edit-link">', '</span>' );
			?>
		</div>

		<?php the_post_thumbnail() ?>
	</header>

	<section class="entry-content">
		<?php the_content() ?>
	</section>

</article>

