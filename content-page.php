<?php
/**
  * Content
  * @package Wordpress
  * @subpackage veggy
  * @since Veggy 0.0.0
  */
?>

<article id="post-<?php the_ID() ?>" <?php post_class() ?>>

<?php the_post_thumbnail() ?>

<?php the_title( '<header class="entry-header">
	<h1 class="entry-title">', '</h1></header>' )?>


<section class="entry-content">
	<?php the_content() ?>
</section>