<?php
/**
 * The template for displaying featured content
 *
 * @package WordPress
 * @subpackage Veggy
 * @since Veggy 0.0.1
 */
?>

<?php do_action('twentyfourteen_featured_posts_before') ?>

<?php $featured_posts = twentyfourteen_get_featured_posts() ?>

<?php foreach ((array) $featured_posts as $order => $post) : ?>

	<?php setup_postdata($post) ?>

	<li id="post-<?php the_ID() ?>" <?php post_class() ?>>

		<a class="post-thumbnail" href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail() ?>
			<h2><?php the_title() ?></h2>
		</a>
	</li>


<?php endforeach ?>

<?php do_action('twentyfourteen_featured_posts_after') ?>

<?php wp_reset_postdata() ?>
