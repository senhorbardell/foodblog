<?php
/** 
	* Main template file
	* @package Wordpress
	* @subpackage veggy
	* @since Veggy 0.0.0
	*/
?>

<?php get_header() ?>

<section class="main">

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

				<?php get_template_part('content', get_post_format()) ?>

		<?php endwhile ?>

		<?php veggy_pagination() ?>

	<?php else : ?>

		<?php get_template_part('content', 'none') ?>

	<?php endif ?>

</section>

<?php get_footer() ?>