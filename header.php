<?php
/**
  * Header
  * @package Wordpress
  * @subpackage veggy
  * @since Veggy 0.0.0
  */
?>

<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php wp_head(); ?>
		<link href='http://fonts.googleapis.com/css?family=PT+Sans|Poiret+One|Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	</head>

	<body <?php body_class(); ?>>

		<?php get_template_part('nav', 'header') ?>

	    <section class="container">